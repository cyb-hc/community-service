package com.zy.community.web.controller.mini.login.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 注册Dto
 */
public class RegisterDto implements Serializable {

    //openId
    @NotBlank(message = "openId不能为空")
    private String openId;
    //手机号
    @NotBlank(message = "手机号不能为空")
    private String phoneNum;
    //头像
    private String avatar;
    //昵称
    private String nickName;
    //生日
    private Date birthday;
    //微信性别定义 0 未知,1男，2 女
    private Integer gender;
    //验证码 不会持久化
    @NotBlank(message = "验证码不能为空")
    private String code;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
