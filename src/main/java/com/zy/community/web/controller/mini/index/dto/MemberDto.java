package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * 成员DTO
 */
public class MemberDto implements Serializable {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    private String realName;

    private String phoneNum;

    private String ownerType;

    private boolean canUnBind;//能否解绑


    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public boolean isCanUnBind() {
        return canUnBind;
    }

    public void setCanUnBind(boolean canUnBind) {
        this.canUnBind = canUnBind;
    }
}
