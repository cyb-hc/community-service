package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.community.domain.vo.ComplaintSuggestType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 投诉建议Dto
 */
public class ComplaintSuggestionDto implements Serializable {
    /**
     * 社区Id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotNull(message = "社区Id不能为空")
    private Long communityId;
    /**
     * 投诉建议类型
     */
    @NotNull(message = "类型不能为空")
    private ComplaintSuggestType complaintSuggestType;
    /**
     * 投诉内容
     */
    @NotBlank(message = "投诉内容不能为空")
    private String complaintSuggestContent;

    /**
     * 投诉或者建议的图片
     */
    private List<String> imageUrls = new ArrayList<>();



    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public ComplaintSuggestType getComplaintSuggestType() {
        return complaintSuggestType;
    }

    public void setComplaintSuggestType(ComplaintSuggestType complaintSuggestType) {
        this.complaintSuggestType = complaintSuggestType;
    }

    public String getComplaintSuggestContent() {
        return complaintSuggestContent;
    }

    public void setComplaintSuggestContent(String complaintSuggestContent) {
        this.complaintSuggestContent = complaintSuggestContent;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }
}
