package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.dto.ZyOwnerRoomDto;
import com.zy.community.community.domain.dto.ZyOwnerRoomRecordDto;
import com.zy.community.community.service.IZyOwnerRoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 房屋绑定 Controller
 *
 * @author yangdi
 * @date 2020-12-15
 */
@Api(tags = "房屋绑定")
@RestController
@RequestMapping("/system/ownerRoom")
public class ZyOwnerRoomController extends BaseController {
    @Resource
    private IZyOwnerRoomService zyOwnerRoomService;

    /**
     * 查询房屋绑定 列表
     */
    @ApiOperation(value = "查询房屋绑定列表")
    @PreAuthorize("@ss.hasPermi('system:room:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyOwnerRoom zyOwnerRoom) {
        startPage();
        List<ZyOwnerRoomDto> list = zyOwnerRoomService.selectZyOwnerRoomList(zyOwnerRoom);
        return getDataTable(list);
    }

    /**
     * 审核
     */
    @ApiOperation(value = "审核")
    @PreAuthorize("@ss.hasPermi('system:room:edit')")
    @Log(title = "审核 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyOwnerRoomRecord zyOwnerRoomRecord) {
        return toAjax(zyOwnerRoomService.updateZyOwnerRoom(zyOwnerRoomRecord));
    }

    /**
     * 查询审核记录 列表
     */
    @ApiOperation(value = "查询审核记录")
    @PreAuthorize("@ss.hasPermi('system:room:list')")
    @GetMapping(value = "/{ownerRoomId}")
    public TableDataInfo queryOwnerRoomRecordByOwnerRoomId(@PathVariable("ownerRoomId") Long ownerRoomId) {
        startPage();
        List<ZyOwnerRoomRecordDto> list = zyOwnerRoomService.queryOwnerRoomRecordByOwnerRoomId(ownerRoomId);
        return getDataTable(list);
    }
}
