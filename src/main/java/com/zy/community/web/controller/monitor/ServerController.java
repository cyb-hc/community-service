package com.zy.community.web.controller.monitor;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.framework.web.domain.Server;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 * 
 * @author yangdi
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController
{
    @PreAuthorize("@ss.hasPermi('monitor:server:list')")
    @GetMapping()
    public ZyResult getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return ZyResult.data(server);
    }
}
