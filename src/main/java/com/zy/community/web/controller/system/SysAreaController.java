package com.zy.community.web.controller.system;

import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.system.service.ISysAreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author yangdi
 */
@Api(tags = "三级联动")
@RestController
@RequestMapping("/system/area")
public class SysAreaController extends BaseController {
    @Resource
    private ISysAreaService iSysAreaService;

    @ApiOperation(value = "查询")
    @GetMapping("/tree")
    public ZyResult getAreaTree(){
        return ZyResult.data(iSysAreaService.findRootArea());
    }
}
