package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyUnit;
import com.zy.community.community.domain.dto.ZyUnitDto;
import com.zy.community.community.service.IZyUnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 单元 Controller
 *
 * @author ruoyi
 * @date 2020-12-11
 */
@Api(tags = "单元")
@RestController
@RequestMapping("/system/unit")
public class ZyUnitController extends BaseController {
    @Resource
    private IZyUnitService zyUnitService;

    /**
     * 查询单元 列表
     */
    @ApiOperation(value = "查询单元")
    @PreAuthorize("@ss.hasPermi('system:unit:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyUnit zyUnit) {
        startPage();
        List<ZyUnitDto> list = zyUnitService.selectZyUnitList(zyUnit);
        return getDataTable(list);
    }

    /**
     * 导出单元 列表
     */
    @ApiOperation(value = "导出单元")
    @PreAuthorize("@ss.hasPermi('system:unit:export')")
    @Log(title = "单元 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyUnit zyUnit) {
        List<ZyUnitDto> list = zyUnitService.selectZyUnitList(zyUnit);
        ExcelUtil<ZyUnitDto> util = new ExcelUtil<ZyUnitDto>(ZyUnitDto.class);
        return util.exportExcel(list, "unit");
    }

    /**
     * 获取单元 详细信息
     */
    @ApiOperation(value = "获取单元")
    @PreAuthorize("@ss.hasPermi('system:unit:query')")
    @GetMapping(value = "/{unitId}")
    public ZyResult getInfo(@PathVariable("unitId") Long unitId) {
        return ZyResult.data(zyUnitService.selectZyUnitById(unitId));
    }

    /**
     * 新增单元
     */
    @ApiOperation(value = "新增单元")
    @PreAuthorize("@ss.hasPermi('system:unit:add')")
    @Log(title = "单元 ", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyUnit zyUnit) {
        return toAjax(zyUnitService.insertZyUnit(zyUnit));
    }

    /**
     * 修改单元
     */
    @ApiOperation(value = "修改单元")
    @PreAuthorize("@ss.hasPermi('system:unit:edit')")
    @Log(title = "单元 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyUnit zyUnit) {
        return toAjax(zyUnitService.updateZyUnit(zyUnit));
    }

    /**
     * 删除单元
     */
    @ApiOperation(value = "删除单元")
    @PreAuthorize("@ss.hasPermi('system:unit:remove')")
    @Log(title = "单元 ", businessType = BusinessType.DELETE)
    @DeleteMapping("/{unitIds}")
    public ZyResult remove(@PathVariable Long[] unitIds) {
        return toAjax(zyUnitService.deleteZyUnitByIds(unitIds));
    }
}
