package com.zy.community.mini.factory;

import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.framework.security.mini.MiniContextUtils;
import com.zy.community.web.controller.mini.index.dto.ComplaintSuggestionDto;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class ComplaintSuggestionFactory {
    @Resource
    private ZyOwnerMapper zyOwnerMapper;

    public ZyComplaintSuggest createByDto(ComplaintSuggestionDto dto){
        if (dto==null) return null;
        ZyComplaintSuggest zyComplaintSuggest = new ZyComplaintSuggest();
        zyComplaintSuggest.setCommunityId(dto.getCommunityId());
        zyComplaintSuggest.setUserId(zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId()));
        zyComplaintSuggest.setComplaintSuggestContent(dto.getComplaintSuggestContent());
        zyComplaintSuggest.setComplaintSuggestType(dto.getComplaintSuggestType());
        zyComplaintSuggest.setCreateTime(new Date());
        return zyComplaintSuggest;
    }
}
