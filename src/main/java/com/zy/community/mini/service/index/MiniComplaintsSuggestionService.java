package com.zy.community.mini.service.index;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.mapper.ZyComplaintSuggestMapper;
import com.zy.community.community.mapper.ZyFilesMapper;
import com.zy.community.mini.factory.ComplaintSuggestionFactory;
import com.zy.community.web.controller.mini.index.dto.ComplaintSuggestionDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 投诉建议服务
 */
@Service
public class MiniComplaintsSuggestionService {
    @Resource
    private ZyComplaintSuggestMapper zyComplaintSuggestMapper;

    @Resource
    private ComplaintSuggestionFactory complaintSuggestionFactory;
    @Resource
    private ZyFilesMapper zyFilesMapper;

    @Transactional
    public ZyResult<String> saveComplainSuggestion(ComplaintSuggestionDto dto){
        ZyComplaintSuggest byDto = complaintSuggestionFactory.createByDto(dto);
        zyComplaintSuggestMapper.insert(byDto);
        if (dto.getImageUrls().size()>0){
            List<ZyFiles> files = new ArrayList<>();
            DefaultIdentifierGenerator defaultIdentifierGenerator = new DefaultIdentifierGenerator();
            dto.getImageUrls().forEach(url->{
                ZyFiles zyFiles = new ZyFiles();
                zyFiles.setFilesId(defaultIdentifierGenerator.nextId(null));
                zyFiles.setFilesUrl(url);
                zyFiles.setCreateTime(new Date());
                zyFiles.setSource(0);//来源0APP端，1PC端
                zyFiles.setUserId(byDto.getUserId());
                zyFiles.setParentId(byDto.getComplaintSuggestId());//设置图片所属
                zyFiles.setRemark("ComplaintSuggest");
                files.add(zyFiles);
            });
            zyFilesMapper.insertFilesBatch(files);
        }

        return ZyResult.success("保存成功");
    }


}
