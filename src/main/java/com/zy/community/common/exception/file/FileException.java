package com.zy.community.common.exception.file;

import com.zy.community.common.exception.BaseException;

/**
 * 文件信息异常类
 * 
 * @author yangdi
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

}
