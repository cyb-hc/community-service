package com.zy.community.community.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyRepair;
import com.zy.community.community.domain.dto.ZyRepairDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 报修信息Mapper接口
 * 
 * @author yin
 * @date 2020-12-22
 */
public interface ZyRepairMapper extends BaseMapper<ZyRepair>
{
    @Select("<script>" +
            "select a.repair_id,a.repair_num,a.repair_state, a.assignment_time," +
            " a.cancel_time, a.door_time, a.create_time, a.complete_phone," +
            " a.complete_name, a.remark, a.create_time, a.repair_content," +
            " a.receiving_orders_time,a.address," +
            " b.owner_real_name as ownerRealName,b.owner_phone_number as ownerPhoneNumber" +
            " from zy_repair a LEFT JOIN zy_owner b on a.user_id = b.owner_id " +
            "<where>" +
            "<if test=\"repairState != null\"> and a.repair_state = #{repairState}</if>" +
            "<if test=\"ownerPhoneNumber != null and ownerPhoneNumber != ''\"> and b.owner_phone_number = #{ownerPhoneNumber}</if>" +
            "<if test=\"ownerRealName != null and ownerRealName != ''\"> and b.owner_real_name like concat ('%',#{ownerRealName},'%')</if>" +
            "and a.community_id = #{communityId}" +
            "</where>" +
            " order by a.create_time DESC"+
            "</script>")
    List<ZyRepairDto> selectZyRepairById(ZyRepairDto zyRepairDto);
}
