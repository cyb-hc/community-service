package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyRoom;
import com.zy.community.community.domain.dto.ZyRoomDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 房间 Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-11
 */
public interface ZyRoomMapper extends BaseMapper<ZyRoom> {

    /**
     * 查询列表
     * @param zyRoom
     * @return
     */
    @Select("<script>" +
            "SELECT d.room_id,d.community_id,d.building_id," +
            "d.unit_id,d.room_level,d.room_code," +
            "d.room_name,d.room_acreage,d.room_cost," +
            "d.room_status,d.room_is_shop,d.room_s_commercial_house," +
            "d.room_house_type,d.create_by,d.create_time," +
            "d.update_by,d.update_time,d.remark," +
            "a.community_name as communityName,b.building_name as buildingName,c.unit_name as unitName " +
            "FROM zy_room d " +
            "LEFT JOIN zy_community a on a.community_id = d.community_id " +
            "LEFT JOIN zy_building b on b.building_id = d.building_id " +
            "LEFT JOIN zy_unit c on c.unit_id = d.unit_id " +
            "<where>" +
            "<if test=\"roomName !=null and roomName != ''\">" +
            "and d.room_name like concat('%',#{roomName},'%') " +
            "</if>" +
            "<if test=\"unitId !=null and unitId != ''\">" +
            "and d.unit_id = #{unitId} " +
            "</if>" +
            "<if test=\"buildingId !=null and buildingId != ''\">" +
            "and d.building_id = #{buildingId} " +
            "</if>" +
            "and d.community_id = #{communityId}" +
            "${params.dataScope}" +
            "</where>" +
            "</script>")
    public List<ZyRoomDto> selectZyRoomList(ZyRoom zyRoom);
}
