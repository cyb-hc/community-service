package com.zy.community.community.service;

import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.domain.dto.ZyComplaintSuggestDto;

import java.util.List;

/**
 * 投诉建议 Service接口
 *
 * @author yangdi
 * @date 2020-12-18
 */
public interface IZyComplaintSuggestService {
    /**
     * 查询投诉建议 列表
     *
     * @param zyComplaintSuggest 投诉建议
     * @return 投诉建议 集合
     */
    public List<ZyComplaintSuggestDto> selectZyComplaintSuggestList(ZyComplaintSuggest zyComplaintSuggest);

}
