package com.zy.community.community.service;

import com.zy.community.community.domain.ZyBuilding;
import com.zy.community.community.domain.dto.KnDto;
import com.zy.community.community.domain.dto.ZyBuildingDto;

import java.util.List;

/**
 * 楼栋 Service接口
 * 
 * @author yangdi
 * @date 2020-12-11
 */
public interface IZyBuildingService 
{
    /**
     * 查询楼栋 
     * 
     * @param buildingId 楼栋 ID
     * @return 楼栋 
     */
    public ZyBuilding selectZyBuildingById(Long buildingId);

    /**
     * 查询楼栋 列表
     * 
     * @param zyBuilding 楼栋 
     * @return 楼栋 集合
     */
    public List<ZyBuildingDto> selectZyBuildingList(ZyBuilding zyBuilding);

    /**
     * 新增楼栋 
     * 
     * @param zyBuilding 楼栋 
     * @return 结果
     */
    public int insertZyBuilding(ZyBuilding zyBuilding);

    /**
     * 修改楼栋 
     * 
     * @param zyBuilding 楼栋 
     * @return 结果
     */
    public int updateZyBuilding(ZyBuilding zyBuilding);

    /**
     * 批量删除楼栋 
     * 
     * @param buildingIds 需要删除的楼栋 ID
     * @return 结果
     */
    public int deleteZyBuildingByIds(Long[] buildingIds);

    /**
     * 删除楼栋 信息
     * 
     * @param buildingId 楼栋 ID
     * @return 结果
     */
    public int deleteZyBuildingById(Long buildingId);

    /**
     * 房间新增所需下拉
     * @param zyBuilding
     * @return
     */
    List<KnDto> queryPullDownRoom(ZyBuilding zyBuilding);
}
