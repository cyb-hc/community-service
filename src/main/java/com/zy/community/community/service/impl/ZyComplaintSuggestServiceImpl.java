package com.zy.community.community.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.domain.dto.ZyCommunityInteractionDto;
import com.zy.community.community.domain.dto.ZyComplaintSuggestDto;
import com.zy.community.community.mapper.ZyComplaintSuggestMapper;
import com.zy.community.community.service.IZyComplaintSuggestService;
import com.zy.community.community.service.IZyFilesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 投诉建议 Service业务层处理
 *
 * @author yangdi
 * @date 2020-12-18
 */
@Service
public class ZyComplaintSuggestServiceImpl implements IZyComplaintSuggestService {
    @Resource
    private ZyComplaintSuggestMapper zyComplaintSuggestMapper;

    @Value("${community.value}")
    private String propertyId;

    @Resource
    private IZyFilesService zyFilesService;
    /**
     * 查询投诉建议 列表
     *
     * @param zyComplaintSuggest 投诉建议
     * @return 投诉建议
     */
    @Override
    public List<ZyComplaintSuggestDto> selectZyComplaintSuggestList(ZyComplaintSuggest zyComplaintSuggest) {
        zyComplaintSuggest.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        List<ZyComplaintSuggestDto> suggestList = zyComplaintSuggestMapper.selectZyComplaintSuggestList(zyComplaintSuggest);
        for (ZyComplaintSuggestDto zyComplaintSuggestDto : suggestList) {
            Long parentId = zyComplaintSuggestDto.getComplaintSuggestId();
            List wyglFilesList = zyFilesService.getWyglFilesList(parentId);
            zyComplaintSuggestDto.setUrlList(wyglFilesList);
        }
        return suggestList;
    }

}
