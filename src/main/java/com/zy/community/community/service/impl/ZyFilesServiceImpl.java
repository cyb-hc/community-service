package com.zy.community.community.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.SecurityUtils;
import com.zy.community.common.utils.StringUtils;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.mapper.ZyFilesMapper;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.service.IZyFilesService;
import com.zy.community.framework.security.mini.MiniContextUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件管理Service业务层处理
 * 
 * @author yin
 * @date 2020-12-17
 */
@Service
public class ZyFilesServiceImpl implements IZyFilesService
{
    @Resource
    private ZyFilesMapper zyFilesMapper;
    @Resource
    private ZyOwnerMapper zyOwnerMapper;

    /**
     * 查询文件管理
     * 
     * @param filesId 文件管理ID
     * @return 文件管理
     */
    @Override
    public ZyFiles selectZyFilesById(Long filesId)
    {
        return zyFilesMapper.selectById(filesId);
    }

    /**
     * 查询文件管理列表
     * 
     * @param zyFiles 文件管理
     * @return 文件管理
     */
    @Override
    public List<ZyFiles> selectZyFilesList(ZyFiles zyFiles)
    {
        return zyFilesMapper.selectList(new QueryWrapper<ZyFiles>());
    }

    /**
     * 新增文件管理
     * 
     * @param zyFiles 文件管理
     * @return 结果
     */
    @Override
    public int insertZyFiles(ZyFiles[] zyFiles)
    {
        int str = 0;
        if(null == zyFiles || zyFiles.length == 0){
            return str;
        }
        for (ZyFiles wyglFile : zyFiles) {
            wyglFile.setCreateTime(DateUtils.getNowDate());
            if(null != wyglFile.getSource()){
                if(wyglFile.getSource() == 0){//移动端
                    Long ownerId = zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId());
                    wyglFile.setUserId(ownerId);
                }else if(wyglFile.getSource() == 1){//web端
                    wyglFile.setUserId(SecurityUtils.getUserId());
                }
            }
            str += zyFilesMapper.insert(wyglFile);

        }
        return str;
    }

    /**
     * 修改文件管理
     * 
     * @param zyFiles 文件管理
     * @return 结果
     */
    @Override
    public int updateZyFiles(ZyFiles zyFiles)
    {
        zyFiles.setUpdateTime(DateUtils.getNowDate());
        if(null != zyFiles.getSource()){
            if(zyFiles.getSource() == 0){//移动端
                zyFiles.setUpdateBy(MiniContextUtils.getPhoneNum());
            }else if(zyFiles.getSource() == 1){//web端
                zyFiles.setUpdateBy(SecurityUtils.getUserId() + "");
            }
        }
        return zyFilesMapper.updateById(zyFiles);
    }

    /**
     * 批量删除文件管理
     * 
     * @param filesIds 需要删除的文件管理ID
     * @return 结果
     */
    @Override
    public int deleteZyFilesByIds(Long[] filesIds)
    {
        return zyFilesMapper.deleteById(filesIds);
    }

    /**
     * 删除文件管理信息
     * 
     * @param filesId 文件管理ID
     * @return 结果
     */
    @Override
    public int deleteZyFilesById(Long filesId)
    {
        return zyFilesMapper.deleteById(filesId);
    }

    /**
     * 根据fileIds获取图片列表
     * @param parentId
     * @return
     */
    @Override
    public List getWyglFilesList(Long parentId){
        if(null == parentId){
            return null;
        }
        List list = new ArrayList();
        /**获取报修的图片列表*/
        List<ZyFiles> zyFiles = zyFilesMapper.selectWyglFilesListParentId(parentId);
        if(null != zyFiles){
            for (ZyFiles wyglFile : zyFiles) {
                list.add(wyglFile.getFilesUrl());
            }
        }
        return list;
    }
}
