package com.zy.community.community.service;

import com.zy.community.community.domain.ZyRoom;
import com.zy.community.community.domain.dto.ZyRoomDto;

import java.util.List;

/**
 * 房间 Service接口
 * 
 * @author ruoyi
 * @date 2020-12-11
 */
public interface IZyRoomService 
{
    /**
     * 查询房间 
     * 
     * @param roomId 房间 ID
     * @return 房间 
     */
    public ZyRoom selectZyRoomById(Long roomId);

    /**
     * 查询房间 列表
     * 
     * @param zyRoom 房间 
     * @return 房间 集合
     */
    public List<ZyRoomDto> selectZyRoomList(ZyRoom zyRoom);

    /**
     * 新增房间 
     * 
     * @param zyRoom 房间 
     * @return 结果
     */
    public int insertZyRoom(ZyRoom zyRoom);

    /**
     * 修改房间 
     * 
     * @param zyRoom 房间 
     * @return 结果
     */
    public int updateZyRoom(ZyRoom zyRoom);

    /**
     * 批量删除房间 
     * 
     * @param roomIds 需要删除的房间 ID
     * @return 结果
     */
    public int deleteZyRoomByIds(Long[] roomIds);

    /**
     * 删除房间 信息
     * 
     * @param roomId 房间 ID
     * @return 结果
     */
    public int deleteZyRoomById(Long roomId);
}
