package com.zy.community.community.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.Gender;
import com.zy.community.community.domain.vo.LogonMode;
import com.zy.community.community.domain.vo.OwnerStatus;

import java.util.Date;

/**
 * @author yangdi
 */
public class ZyOwnerDto extends BaseEntity {
    /**
     * 房屋绑定id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerRoomId;

    /**
     * 小区id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    private String communityName;

    /**
     * 楼栋id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    private String buildingName;

    /**
     * 单元id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    private String unitName;

    /**
     * 房间id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;

    private String roomName;

    /**
     * 业主id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    private String ownerRealName;

    /**
     * 业主类型
     */
    private String ownerType;

    /** 昵称 */
    private String ownerNickname;

    /** 性别0默认1男2女 */
    private Gender ownerGender;

    /** 年龄 */
    private Integer ownerAge;

    /** 身份证号码 */
    private String ownerIdCard;

    /** 手机号码 */
    private String ownerPhoneNumber;

    /** 微信号 */
    private String ownerWechatId;

    /** qq号码 */
    private String ownerQqNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ownerBirthday;

    /** 头像 */
    private String ownerPortrait;

    /** 个性签名 */
    private String ownerSignature;

    /** 禁用状态enable启用-disable禁用 */
    private OwnerStatus ownerStatus;

    /** 注册方式（1微信2app3web） */
    private LogonMode ownerLogonMode;

    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerRealName() {
        return ownerRealName;
    }

    public void setOwnerRealName(String ownerRealName) {
        this.ownerRealName = ownerRealName;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getOwnerNickname() {
        return ownerNickname;
    }

    public void setOwnerNickname(String ownerNickname) {
        this.ownerNickname = ownerNickname;
    }

    public Gender getOwnerGender() {
        return ownerGender;
    }

    public void setOwnerGender(Gender ownerGender) {
        this.ownerGender = ownerGender;
    }

    public Integer getOwnerAge() {
        return ownerAge;
    }

    public void setOwnerAge(Integer ownerAge) {
        this.ownerAge = ownerAge;
    }

    public String getOwnerIdCard() {
        return ownerIdCard;
    }

    public void setOwnerIdCard(String ownerIdCard) {
        this.ownerIdCard = ownerIdCard;
    }

    public String getOwnerPhoneNumber() {
        return ownerPhoneNumber;
    }

    public void setOwnerPhoneNumber(String ownerPhoneNumber) {
        this.ownerPhoneNumber = ownerPhoneNumber;
    }

    public String getOwnerWechatId() {
        return ownerWechatId;
    }

    public void setOwnerWechatId(String ownerWechatId) {
        this.ownerWechatId = ownerWechatId;
    }

    public String getOwnerQqNumber() {
        return ownerQqNumber;
    }

    public void setOwnerQqNumber(String ownerQqNumber) {
        this.ownerQqNumber = ownerQqNumber;
    }

    public Date getOwnerBirthday() {
        return ownerBirthday;
    }

    public void setOwnerBirthday(Date ownerBirthday) {
        this.ownerBirthday = ownerBirthday;
    }

    public String getOwnerPortrait() {
        return ownerPortrait;
    }

    public void setOwnerPortrait(String ownerPortrait) {
        this.ownerPortrait = ownerPortrait;
    }

    public String getOwnerSignature() {
        return ownerSignature;
    }

    public void setOwnerSignature(String ownerSignature) {
        this.ownerSignature = ownerSignature;
    }

    public OwnerStatus getOwnerStatus() {
        return ownerStatus;
    }

    public void setOwnerStatus(OwnerStatus ownerStatus) {
        this.ownerStatus = ownerStatus;
    }

    public LogonMode getOwnerLogonMode() {
        return ownerLogonMode;
    }

    public void setOwnerLogonMode(LogonMode ownerLogonMode) {
        this.ownerLogonMode = ownerLogonMode;
    }

    @Override
    public String toString() {
        return "ZyOwnerDto{" +
                "ownerRoomId=" + ownerRoomId +
                ", communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", buildingId=" + buildingId +
                ", buildingName='" + buildingName + '\'' +
                ", unitId=" + unitId +
                ", unitName='" + unitName + '\'' +
                ", roomId=" + roomId +
                ", roomName='" + roomName + '\'' +
                ", ownerId=" + ownerId +
                ", ownerRealName='" + ownerRealName + '\'' +
                ", ownerType='" + ownerType + '\'' +
                ", ownerNickname='" + ownerNickname + '\'' +
                ", ownerGender=" + ownerGender +
                ", ownerAge=" + ownerAge +
                ", ownerIdCard='" + ownerIdCard + '\'' +
                ", ownerPhoneNumber='" + ownerPhoneNumber + '\'' +
                ", ownerWechatId='" + ownerWechatId + '\'' +
                ", ownerQqNumber='" + ownerQqNumber + '\'' +
                ", ownerBirthday=" + ownerBirthday +
                ", ownerPortrait='" + ownerPortrait + '\'' +
                ", ownerSignature='" + ownerSignature + '\'' +
                ", ownerStatus=" + ownerStatus +
                ", ownerLogonMode=" + ownerLogonMode +
                '}';
    }
}
