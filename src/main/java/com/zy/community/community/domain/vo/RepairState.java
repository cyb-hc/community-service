package com.zy.community.community.domain.vo;

public enum RepairState {
    /************************报修状态*********************************/
    /**
     * 待处理
     */
    Pending,

    /**
     * 已分派
     */
    Allocated,

    /**
     * 处理中
     */
    Processing,

    /**
     * 已处理
     */
    Processed,

    /**
     * 不处理
     */
    No_Processed,

    /**
     * 已取消
     */
    Cancelled,
}
