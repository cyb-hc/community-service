package com.zy.community.community.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 房间 对象 zy_room
 * 
 * @author yangdi
 * @date 2020-12-11
 */
public class ZyRoom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 房间id */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Excel(name = "房间id")
    private Long roomId;

    /** 小区id */
    @Excel(name = "小区id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /** 楼栋id */
    @Excel(name = "楼栋id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    /** 单元id */
    @Excel(name = "单元id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    /** 楼层 */
    @Excel(name = "楼层")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomLevel;

    /** 房间编号 */
    @Excel(name = "房间编号")
    private String roomCode;

    /** 房间名称 */
    @Excel(name = "房间名称")
    private String roomName;

    /** 房屋建筑面积 */
    @Excel(name = "房屋建筑面积")
    private BigDecimal roomAcreage;

    /** 算费系数 */
    @Excel(name = "算费系数")
    private BigDecimal roomCost;

    /** 房屋状态 */
    @Excel(name = "房屋状态")
    private String roomStatus;

    /** 是否商铺 */
    @Excel(name = "是否商铺")
    private String roomIsShop;

    /** 是否商品房 */
    @Excel(name = "是否商品房")
    private String roomSCommercialHouse;

    /** 房屋户型 */
    @Excel(name = "房屋户型")
    private String roomHouseType;

    public void setRoomId(Long roomId) 
    {
        this.roomId = roomId;
    }

    public Long getRoomId() 
    {
        return roomId;
    }
    public void setCommunityId(Long communityId) 
    {
        this.communityId = communityId;
    }

    public Long getCommunityId() 
    {
        return communityId;
    }
    public void setBuildingId(Long buildingId) 
    {
        this.buildingId = buildingId;
    }

    public Long getBuildingId() 
    {
        return buildingId;
    }
    public void setUnitId(Long unitId) 
    {
        this.unitId = unitId;
    }

    public Long getUnitId() 
    {
        return unitId;
    }
    public void setRoomLevel(Long roomLevel) 
    {
        this.roomLevel = roomLevel;
    }

    public Long getRoomLevel() 
    {
        return roomLevel;
    }
    public void setRoomCode(String roomCode) 
    {
        this.roomCode = roomCode;
    }

    public String getRoomCode() 
    {
        return roomCode;
    }
    public void setRoomAcreage(BigDecimal roomAcreage) 
    {
        this.roomAcreage = roomAcreage;
    }

    public BigDecimal getRoomAcreage() 
    {
        return roomAcreage;
    }
    public void setRoomCost(BigDecimal roomCost) 
    {
        this.roomCost = roomCost;
    }

    public BigDecimal getRoomCost() 
    {
        return roomCost;
    }
    public void setRoomStatus(String roomStatus) 
    {
        this.roomStatus = roomStatus;
    }

    public String getRoomStatus() 
    {
        return roomStatus;
    }
    public void setRoomIsShop(String roomIsShop) 
    {
        this.roomIsShop = roomIsShop;
    }

    public String getRoomIsShop() 
    {
        return roomIsShop;
    }
    public void setRoomSCommercialHouse(String roomSCommercialHouse) 
    {
        this.roomSCommercialHouse = roomSCommercialHouse;
    }

    public String getRoomSCommercialHouse() 
    {
        return roomSCommercialHouse;
    }
    public void setRoomHouseType(String roomHouseType) 
    {
        this.roomHouseType = roomHouseType;
    }

    public String getRoomHouseType() 
    {
        return roomHouseType;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("roomId", getRoomId())
            .append("communityId", getCommunityId())
            .append("buildingId", getBuildingId())
            .append("unitId", getUnitId())
            .append("roomLevel", getRoomLevel())
            .append("roomCode", getRoomCode())
            .append("roomAcreage", getRoomAcreage())
            .append("roomCost", getRoomCost())
            .append("roomStatus", getRoomStatus())
            .append("roomIsShop", getRoomIsShop())
            .append("roomSCommercialHouse", getRoomSCommercialHouse())
            .append("roomHouseType", getRoomHouseType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
