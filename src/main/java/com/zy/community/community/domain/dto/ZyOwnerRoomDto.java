package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.RoomStatus;

/**
 * @author yangdi
 */
public class ZyOwnerRoomDto extends BaseEntity {

    /**
     * 房屋绑定id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerRoomId;

    /**
     * 小区id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    private String communityName;

    /**
     * 楼栋id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    private String buildingName;

    /**
     * 单元id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    private String unitName;

    /**
     * 房间id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;

    private String roomName;

    /**
     * 业主id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    private String ownerRealName;

    /**
     * 业主类型
     */
    private String ownerType;

    /**
     * 绑定状态（0审核中 1绑定 2审核失败）
     */
    private RoomStatus roomStatus;

    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerRealName() {
        return ownerRealName;
    }

    public void setOwnerRealName(String ownerRealName) {
        this.ownerRealName = ownerRealName;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    @Override
    public String toString() {
        return "ZyOwnerRoomDto{" +
                "ownerRoomId=" + ownerRoomId +
                ", communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", buildingId=" + buildingId +
                ", buildingName='" + buildingName + '\'' +
                ", unitId=" + unitId +
                ", unitName='" + unitName + '\'' +
                ", roomId=" + roomId +
                ", roomName='" + roomName + '\'' +
                ", ownerId=" + ownerId +
                ", ownerRealName='" + ownerRealName + '\'' +
                ", ownerType='" + ownerType + '\'' +
                ", roomStatus=" + roomStatus +
                '}';
    }
}
